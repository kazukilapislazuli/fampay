const asyncHandler = require('../../middleware/async');
const ErrorResponse = require('../../util/errorResponse');
const API_KEY = require('./model');

exports.getAPIKeys = asyncHandler(async (req, res, next) => {
    const apiKeys = await API_KEY.find({isExhausted: false});
    res.status(200).json({
        success: true,
        length: apiKeys.length,
        data: apiKeys,
    });
});

exports.createAPIKey = asyncHandler(async (req, res, next) => {
    const apiKey = await API_KEY.create(req.body);
    res.status(201).json({
        success: true,
        length: apiKey.length,
        data: apiKey,
    })
})

exports.updateAPIKey = asyncHandler(async (req, res, next) => {
    const apiKey = await API_KEY.find({key: req.body.key});
    if (apiKey.length === 0) {
        return next(new ErrorResponse(`API Key ${req.body.key} Not Found `, 404));
    }
    const updatedAPIKey = await API_KEY.findOneAndUpdate({key: req.body.key}, {isExhausted: req.body.isExhausted}, {
        new: true,
        runValidators: true,
    });

    res.status(200).json({
        success: true,
        data: updatedAPIKey,
    });
})
