const express = require('express');
const {getAPIKeys, createAPIKey, updateAPIKey} = require("./controller");
const router = express.Router();

router.route('/').get(getAPIKeys).post(createAPIKey);
router.route('/update').patch(updateAPIKey);

module.exports = router;
