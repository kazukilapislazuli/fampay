const mongoose = require('mongoose');
const apiKeysSchema = new mongoose.Schema({
    key: {
        type: String,
        unique: true,
        required: true,
    },
    isExhausted: {
        type: Boolean,
        default: false,
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})
module.exports = mongoose.model('API_KEY', apiKeysSchema);
