const mongoose = require('mongoose');
const videoSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    etag: {
        type: String,
        required: true,
    },
    channelId: {

        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    publishedAt: {
        type: Date,
        required: true,
    },
    videoId: {
        type: String,
        required: true,
    },
    thumbnails: {
        default: {
            url: {
                type: String,
                required: true,
            },
            width: {
                type: Number,
                required: true,
            },
            height: {
                type: Number,
                required: true,
            }
        },
        medium: {
            url: {
                type: String,
                required: true,
            },
            width: {
                type: Number,
                required: true,
            },
            height: {
                type: Number,
                required: true,
            }
        },
        high: {
            url: {
                type: String,
                required: true,
            },
            width: {
                type: Number,
                required: true,
            },
            height: {
                type: Number,
                required: true,
            }
        }
    }
})

module.exports = mongoose.model('Video', videoSchema);
