const axios = require('axios');
const asyncHandler = require('../../middleware/async');
const Video = require('./model')

exports.getVideos = asyncHandler(async (req, res, next) => {
    try {
        const {
            page = 0,
            perPage = 10,
            sort = '-publishedAt',
        } = req.query;
        const skip = page * perPage;
        const videos = await Video.find().limit(perPage).skip(skip).sort(sort);
        const totalVideos = await Video.countDocuments();
        res.status(200).json({
            success: true,
            data: videos,
            currentPage: page,
            totalPages: Math.ceil(totalVideos / perPage),
            totalVideos: totalVideos,
        });
    } catch (e) {
        console.log(`Error in videoController.getVideos : ${e}`);
        return res.status(500).json({
            success: false,
            error: e.message,
        })
    }
})
