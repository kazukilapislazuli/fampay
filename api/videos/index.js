const express = require("express");
const { getVideos } = require("./controller");

const router = express.Router()

router.route("/").get(getVideos);

module.exports = router;
