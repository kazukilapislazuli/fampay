const cron = require('node-cron');

const config = require("../config/yt.config.json");
const axios = require("axios");
const dayjs = require('dayjs')

const BASE_URL = config.API_URL;
const API_KEY = config.API_KEYS[0]

const Video = require('../api/videos/model')

const getAllVideos = async () => {
    try {
        const publishedAfter = dayjs().subtract(1, 'minute').toISOString();
        const url = `${BASE_URL}/search?key=${API_KEY}&part=snippet&publishedAfter=${publishedAfter}&q=${config.OPTIONS.yt_search_category}&maxResults=${config.OPTIONS.yt_max_result_per_query}`
        const response = await axios.get(url)
        console.log(response.data)
        return response.data;
    } catch (e) {
        return (`Error ${e}`)
    }
}

const insertInDB = async () => {
    try {
        const data = await getAllVideos();
        // console.log(data)
        if (data && data.items && data.items.length > 0) {
            const items = data.items;
            const videos = []
            items.forEach((item) => {
                const {
                    id: {videoId},
                    etag: etag,
                    snippet: {publishedAt, channelId, title, description, thumbnails},
                } = item;

                videos.push({
                    title: title,
                    channelId: channelId,
                    videoId: videoId,
                    etag: etag,
                    description: description,
                    publishedAt: publishedAt,
                    thumbnails: thumbnails,
                });
            });
            console.log(`${videos.length} Items Inserted`)
            await Video.create(videos);
        }

        return "Success"
    } catch (e) {
        return "Error : " + e;
    }

}

const updateVideoTaskSchedule = cron.schedule(
    '* * * * *',
    async () => {
        console.log("resp1")
        const response = await insertInDB();
        console.log("CRON JOB to Update Video Ran Successfully : ", response);
    },
    {
        scheduled: false,
    }
)

module.exports = updateVideoTaskSchedule;
