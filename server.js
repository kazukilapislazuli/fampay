const express = require('express');
const errorHandler = require('./middleware/error');
const mongoSanitize = require('express-mongo-sanitize');
const xss = require('xss-clean');
const dotenv = require('dotenv')
const cors = require('cors');
const hpp = require('hpp');
const fileUpload = require('express-fileupload');
const rateLimit = require('express-rate-limit');
const cronJobs = require('./cron');

// load env variables
require('dotenv').config();

// Import DB
const connectDB = require('./config/db');
connectDB();
require('colors');

// route files
const videos = require('./api/videos');
const apiKeys = require('./api/apiKeys');

const app = express();
// Body Parser

app.use(express.json());
// sanitize Data

app.use(mongoSanitize());

// xss-clean

app.use(xss());
// Rate Limit

const limiter = rateLimit({
    windowMs: 60 * 1000, // 1 minutes
    max: 10000, // limit each IP to 1000 requests per windowMs
});
app.use(limiter);
// hpp

app.use(hpp());
// cors

app.use(cors());

app.options('*', cors());

// file Upload
app.use(fileUpload());


// Use Routes
app.use('/api/v1/videos', videos);
app.use('/api/v1/apiKey', apiKeys);


app.use(errorHandler);
const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`.yellow.bold);
    cronJobs.updateVideoTaskSchedule.start()
});

// // Handle unhandled promise rejections
// process.on('unhandledRejection', (err, promise) => {
//     console.log(`Error: ${err.message}`.red);
// });


module.exports = app;
