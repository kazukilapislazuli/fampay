# Backend-Server-Template
This is the basic template of Backend Server using NodeJS and Express connected with MongoDB. 

## Steps to set up
1. Clone the Repository using ``` git clone git@gitlab.com:kazukilapislazuli/fampay.git ```.
2. In your local system, open terminal at that location and run ``` npm i ```.
3. Add .env file inside the folder along with other files using below structure.
    ```
    PORT=5000
    MONGO_URI=<Your MongoDB URI>
    YOUTUBE_API_KEY=<Your Youtube API Key>
    JWT_SECRET=<SecretString you want to have>
    JWT_EXPIRES_IN=7d
    JWT_COOKIE_EXPIRE=7d

    ```
4. Run ``` npm start ```
5. Navigate to ``` http://localhost:5000 ``` to see the app live.

## Steps to Run the Project with Docker Container
1. Clone the Repository from docker hub using ``` docker pull kazukilapislazuli/fampay ```.
2. Run the Docker Container using ``` docker run -p 5000:5000 kazukilapislazuli/fampay ```.
3. Navigate to ``` http://localhost:5000 ``` to see the app live.


## Using APIs
1. Get All Videos using ``` http://localhost:5000/api/v1/videos ```
2. Move to next page using ``` http://localhost:5000/api/v1/videos?page=<page number> ```
3. Get more item per page using ``` http://localhost:5000/api/v1/videos?perPage=<item per page> ```

 ## Acknowledgement

* Made with &#9829; for Community to fasten development process.
